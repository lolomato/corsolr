package course.web.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.ActionURL;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.omg.CORBA.Context;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.util.ParamUtil;

import course.model.ElencoCorsi;
import course.service.ElencoCorsiLocalServiceUtil;
import course.service.ElencoCorsiServiceUtil;
import course.web.constants.CourseWebPortletKeys;

/**
 * @author utente
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CourseWebPortletKeys.CourseWeb,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CourseWebPortlet extends MVCPortlet {
	
	@Override
		public void doView(RenderRequest renderRequest, RenderResponse renderResponse) //questa funzione viene chiamata in automatico ad ogni render della portlet... in questo momento  non � particolarmente utile
				throws IOException, PortletException {
			// TODO Auto-generated method stub
			super.doView(renderRequest, renderResponse);
		}
	
	public void action(ActionRequest request, ActionResponse response) throws SystemException, PortalException, IOException { //questa � la funzione chiamata alla pressione del pulsante. 
		
		ServiceContext ctx= ServiceContextFactory.getInstance(ElencoCorsi.class.getName(), request); //qui utilizizamo il contesto della portlet per estrapolare dei dati quali l'user id che serviranno a riempire i campi delle tabelle.
		
		ElencoCorsi x=ElencoCorsiServiceUtil.addLibro(ctx.getUserId(), ctx, ParamUtil.getString(request, "CampoNomeCorso"), ParamUtil.getString(request, "CampoNomeProf")); // questa funzione � definita in modules/course/course-service/ElencoCorsiServiceImpl.java riempie i campi della tabella relativa alla portlet. 
		 
		request.setAttribute("varId", x.getPrimaryKey());// questa funzione setta la variabile "varId per passare alla portlet il campo id del valore inserito
			
	
			
		
	}
	

}