create index IX_1B7DFF4F on Unina_ElencoCorsi (nomeCorso[$COLUMN_LENGTH:75$]);
create index IX_6035C61E on Unina_ElencoCorsi (uuid_[$COLUMN_LENGTH:75$], companyId);
create unique index IX_CA153C20 on Unina_ElencoCorsi (uuid_[$COLUMN_LENGTH:75$], groupId);