create table Unina_ElencoCorsi (
	uuid_ VARCHAR(75) null,
	elencoCorsiId LONG not null primary key,
	groupId LONG,
	companyId LONG,
	userId LONG,
	userName VARCHAR(75) null,
	createDate DATE null,
	modifiedDate DATE null,
	nomeCorso VARCHAR(75) null,
	prof VARCHAR(75) null
);