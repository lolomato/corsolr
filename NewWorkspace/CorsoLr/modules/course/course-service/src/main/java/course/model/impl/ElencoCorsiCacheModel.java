/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.model.impl;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import course.model.ElencoCorsi;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing ElencoCorsi in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsi
 * @generated
 */
@ProviderType
public class ElencoCorsiCacheModel implements CacheModel<ElencoCorsi>,
	Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ElencoCorsiCacheModel)) {
			return false;
		}

		ElencoCorsiCacheModel elencoCorsiCacheModel = (ElencoCorsiCacheModel)obj;

		if (elencoCorsiId == elencoCorsiCacheModel.elencoCorsiId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, elencoCorsiId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(21);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", elencoCorsiId=");
		sb.append(elencoCorsiId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", companyId=");
		sb.append(companyId);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", userName=");
		sb.append(userName);
		sb.append(", createDate=");
		sb.append(createDate);
		sb.append(", modifiedDate=");
		sb.append(modifiedDate);
		sb.append(", nomeCorso=");
		sb.append(nomeCorso);
		sb.append(", prof=");
		sb.append(prof);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public ElencoCorsi toEntityModel() {
		ElencoCorsiImpl elencoCorsiImpl = new ElencoCorsiImpl();

		if (uuid == null) {
			elencoCorsiImpl.setUuid("");
		}
		else {
			elencoCorsiImpl.setUuid(uuid);
		}

		elencoCorsiImpl.setElencoCorsiId(elencoCorsiId);
		elencoCorsiImpl.setGroupId(groupId);
		elencoCorsiImpl.setCompanyId(companyId);
		elencoCorsiImpl.setUserId(userId);

		if (userName == null) {
			elencoCorsiImpl.setUserName("");
		}
		else {
			elencoCorsiImpl.setUserName(userName);
		}

		if (createDate == Long.MIN_VALUE) {
			elencoCorsiImpl.setCreateDate(null);
		}
		else {
			elencoCorsiImpl.setCreateDate(new Date(createDate));
		}

		if (modifiedDate == Long.MIN_VALUE) {
			elencoCorsiImpl.setModifiedDate(null);
		}
		else {
			elencoCorsiImpl.setModifiedDate(new Date(modifiedDate));
		}

		if (nomeCorso == null) {
			elencoCorsiImpl.setNomeCorso("");
		}
		else {
			elencoCorsiImpl.setNomeCorso(nomeCorso);
		}

		if (prof == null) {
			elencoCorsiImpl.setProf("");
		}
		else {
			elencoCorsiImpl.setProf(prof);
		}

		elencoCorsiImpl.resetOriginalValues();

		return elencoCorsiImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		elencoCorsiId = objectInput.readLong();

		groupId = objectInput.readLong();

		companyId = objectInput.readLong();

		userId = objectInput.readLong();
		userName = objectInput.readUTF();
		createDate = objectInput.readLong();
		modifiedDate = objectInput.readLong();
		nomeCorso = objectInput.readUTF();
		prof = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(elencoCorsiId);

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(companyId);

		objectOutput.writeLong(userId);

		if (userName == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(userName);
		}

		objectOutput.writeLong(createDate);
		objectOutput.writeLong(modifiedDate);

		if (nomeCorso == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(nomeCorso);
		}

		if (prof == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(prof);
		}
	}

	public String uuid;
	public long elencoCorsiId;
	public long groupId;
	public long companyId;
	public long userId;
	public String userName;
	public long createDate;
	public long modifiedDate;
	public String nomeCorso;
	public String prof;
}