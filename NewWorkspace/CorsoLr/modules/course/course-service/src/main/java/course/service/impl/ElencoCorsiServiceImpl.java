/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service.impl;

import java.util.Date;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.service.ServiceContext;

import course.model.ElencoCorsi;
import course.service.base.ElencoCorsiServiceBaseImpl;

/**
 * The implementation of the elenco corsi remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link course.service.ElencoCorsiService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiServiceBaseImpl
 * @see course.service.ElencoCorsiServiceUtil
 */
public class ElencoCorsiServiceImpl extends ElencoCorsiServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. Always use {@link course.service.ElencoCorsiServiceUtil} to access the elenco corsi remote service.
	 */
	
	@Indexable(type = IndexableType.REINDEX)
	public ElencoCorsi addLibro(long userId, ServiceContext ctx,String nomeCorso,String nomeProf) 
					throws SystemException, PortalException {

		Date now = new Date();
		User user = userLocalService.getUser(userId);

		//validate(titolo, isbn);

		ElencoCorsi ec = elencoCorsiPersistence.create(counterLocalService.increment(ElencoCorsi.class.getName()));
				/*libroPersistence.create(
				counterLocalService.increment(Libro.class.getName()));*/

		ec.setCompanyId(ctx.getCompanyId());
		ec.setGroupId(ctx.getScopeGroupId());
		ec.setUserId(userId);
		ec.setUserName(user.getFullName());
		ec.setCreateDate(ctx.getCreateDate(now));
		ec.setModifiedDate(ctx.getModifiedDate(now));

		//Setting Fields
		
		ec.setNomeCorso(nomeCorso);
		ec.setProf(nomeProf);
		
		
		
		/*libro.setLibreriaId(libreriaId);
		libro.setTitolo(titolo);
		libro.setPubblicato(pubblicato);
		libro.setNumeroPagine(numeroPagine);
		libro.setDataPubblicazione(dataPubblicazione);
		libro.setDescrizione(descrizione);
		libro.setIsbn(isbn);

		// Campi del Workflow
		libro.setStatus(WorkflowConstants.STATUS_DRAFT);
		libro.setStatusByUserId(userId);
		libro.setStatusByUserName(user.getFullName());
		libro.setStatusDate(ctx.getModifiedDate(now));		

		// Custom attributes
		libro.setExpandoBridgeAttributes(ctx);*/

		ec = elencoCorsiPersistence.update(ec);

		/*// Resources
		resourceLocalService.addModelResources(libro, ctx);

		// Asset
		updateAsset(ctx.getUserId(), libro, ctx.getAssetCategoryIds(),
				ctx.getAssetTagNames(), ctx.getAssetLinkEntryIds(),
				ctx.getAssetPriority());

		// Faccio partire il workflow
		WorkflowHandlerRegistryUtil.startWorkflowInstance(libro.getCompanyId(),
				libro.getGroupId(), libro.getUserId(), Libro.class.getName(),
				libro.getPrimaryKey(), libro, ctx);*/

		return ec;
	}
}