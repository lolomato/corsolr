/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.exportimport.kernel.lar.StagedModelType;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link ElencoCorsi}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsi
 * @generated
 */
@ProviderType
public class ElencoCorsiWrapper implements ElencoCorsi,
	ModelWrapper<ElencoCorsi> {
	public ElencoCorsiWrapper(ElencoCorsi elencoCorsi) {
		_elencoCorsi = elencoCorsi;
	}

	@Override
	public Class<?> getModelClass() {
		return ElencoCorsi.class;
	}

	@Override
	public String getModelClassName() {
		return ElencoCorsi.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("elencoCorsiId", getElencoCorsiId());
		attributes.put("groupId", getGroupId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("nomeCorso", getNomeCorso());
		attributes.put("prof", getProf());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long elencoCorsiId = (Long)attributes.get("elencoCorsiId");

		if (elencoCorsiId != null) {
			setElencoCorsiId(elencoCorsiId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String nomeCorso = (String)attributes.get("nomeCorso");

		if (nomeCorso != null) {
			setNomeCorso(nomeCorso);
		}

		String prof = (String)attributes.get("prof");

		if (prof != null) {
			setProf(prof);
		}
	}

	@Override
	public Object clone() {
		return new ElencoCorsiWrapper((ElencoCorsi)_elencoCorsi.clone());
	}

	@Override
	public int compareTo(ElencoCorsi elencoCorsi) {
		return _elencoCorsi.compareTo(elencoCorsi);
	}

	/**
	* Returns the company ID of this elenco corsi.
	*
	* @return the company ID of this elenco corsi
	*/
	@Override
	public long getCompanyId() {
		return _elencoCorsi.getCompanyId();
	}

	/**
	* Returns the create date of this elenco corsi.
	*
	* @return the create date of this elenco corsi
	*/
	@Override
	public Date getCreateDate() {
		return _elencoCorsi.getCreateDate();
	}

	/**
	* Returns the elenco corsi ID of this elenco corsi.
	*
	* @return the elenco corsi ID of this elenco corsi
	*/
	@Override
	public long getElencoCorsiId() {
		return _elencoCorsi.getElencoCorsiId();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _elencoCorsi.getExpandoBridge();
	}

	/**
	* Returns the group ID of this elenco corsi.
	*
	* @return the group ID of this elenco corsi
	*/
	@Override
	public long getGroupId() {
		return _elencoCorsi.getGroupId();
	}

	/**
	* Returns the modified date of this elenco corsi.
	*
	* @return the modified date of this elenco corsi
	*/
	@Override
	public Date getModifiedDate() {
		return _elencoCorsi.getModifiedDate();
	}

	/**
	* Returns the nome corso of this elenco corsi.
	*
	* @return the nome corso of this elenco corsi
	*/
	@Override
	public String getNomeCorso() {
		return _elencoCorsi.getNomeCorso();
	}

	/**
	* Returns the primary key of this elenco corsi.
	*
	* @return the primary key of this elenco corsi
	*/
	@Override
	public long getPrimaryKey() {
		return _elencoCorsi.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _elencoCorsi.getPrimaryKeyObj();
	}

	/**
	* Returns the prof of this elenco corsi.
	*
	* @return the prof of this elenco corsi
	*/
	@Override
	public String getProf() {
		return _elencoCorsi.getProf();
	}

	/**
	* Returns the user ID of this elenco corsi.
	*
	* @return the user ID of this elenco corsi
	*/
	@Override
	public long getUserId() {
		return _elencoCorsi.getUserId();
	}

	/**
	* Returns the user name of this elenco corsi.
	*
	* @return the user name of this elenco corsi
	*/
	@Override
	public String getUserName() {
		return _elencoCorsi.getUserName();
	}

	/**
	* Returns the user uuid of this elenco corsi.
	*
	* @return the user uuid of this elenco corsi
	*/
	@Override
	public String getUserUuid() {
		return _elencoCorsi.getUserUuid();
	}

	/**
	* Returns the uuid of this elenco corsi.
	*
	* @return the uuid of this elenco corsi
	*/
	@Override
	public String getUuid() {
		return _elencoCorsi.getUuid();
	}

	@Override
	public int hashCode() {
		return _elencoCorsi.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _elencoCorsi.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _elencoCorsi.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _elencoCorsi.isNew();
	}

	@Override
	public void persist() {
		_elencoCorsi.persist();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_elencoCorsi.setCachedModel(cachedModel);
	}

	/**
	* Sets the company ID of this elenco corsi.
	*
	* @param companyId the company ID of this elenco corsi
	*/
	@Override
	public void setCompanyId(long companyId) {
		_elencoCorsi.setCompanyId(companyId);
	}

	/**
	* Sets the create date of this elenco corsi.
	*
	* @param createDate the create date of this elenco corsi
	*/
	@Override
	public void setCreateDate(Date createDate) {
		_elencoCorsi.setCreateDate(createDate);
	}

	/**
	* Sets the elenco corsi ID of this elenco corsi.
	*
	* @param elencoCorsiId the elenco corsi ID of this elenco corsi
	*/
	@Override
	public void setElencoCorsiId(long elencoCorsiId) {
		_elencoCorsi.setElencoCorsiId(elencoCorsiId);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_elencoCorsi.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_elencoCorsi.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_elencoCorsi.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the group ID of this elenco corsi.
	*
	* @param groupId the group ID of this elenco corsi
	*/
	@Override
	public void setGroupId(long groupId) {
		_elencoCorsi.setGroupId(groupId);
	}

	/**
	* Sets the modified date of this elenco corsi.
	*
	* @param modifiedDate the modified date of this elenco corsi
	*/
	@Override
	public void setModifiedDate(Date modifiedDate) {
		_elencoCorsi.setModifiedDate(modifiedDate);
	}

	@Override
	public void setNew(boolean n) {
		_elencoCorsi.setNew(n);
	}

	/**
	* Sets the nome corso of this elenco corsi.
	*
	* @param nomeCorso the nome corso of this elenco corsi
	*/
	@Override
	public void setNomeCorso(String nomeCorso) {
		_elencoCorsi.setNomeCorso(nomeCorso);
	}

	/**
	* Sets the primary key of this elenco corsi.
	*
	* @param primaryKey the primary key of this elenco corsi
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_elencoCorsi.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_elencoCorsi.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the prof of this elenco corsi.
	*
	* @param prof the prof of this elenco corsi
	*/
	@Override
	public void setProf(String prof) {
		_elencoCorsi.setProf(prof);
	}

	/**
	* Sets the user ID of this elenco corsi.
	*
	* @param userId the user ID of this elenco corsi
	*/
	@Override
	public void setUserId(long userId) {
		_elencoCorsi.setUserId(userId);
	}

	/**
	* Sets the user name of this elenco corsi.
	*
	* @param userName the user name of this elenco corsi
	*/
	@Override
	public void setUserName(String userName) {
		_elencoCorsi.setUserName(userName);
	}

	/**
	* Sets the user uuid of this elenco corsi.
	*
	* @param userUuid the user uuid of this elenco corsi
	*/
	@Override
	public void setUserUuid(String userUuid) {
		_elencoCorsi.setUserUuid(userUuid);
	}

	/**
	* Sets the uuid of this elenco corsi.
	*
	* @param uuid the uuid of this elenco corsi
	*/
	@Override
	public void setUuid(String uuid) {
		_elencoCorsi.setUuid(uuid);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<ElencoCorsi> toCacheModel() {
		return _elencoCorsi.toCacheModel();
	}

	@Override
	public ElencoCorsi toEscapedModel() {
		return new ElencoCorsiWrapper(_elencoCorsi.toEscapedModel());
	}

	@Override
	public String toString() {
		return _elencoCorsi.toString();
	}

	@Override
	public ElencoCorsi toUnescapedModel() {
		return new ElencoCorsiWrapper(_elencoCorsi.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _elencoCorsi.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof ElencoCorsiWrapper)) {
			return false;
		}

		ElencoCorsiWrapper elencoCorsiWrapper = (ElencoCorsiWrapper)obj;

		if (Objects.equals(_elencoCorsi, elencoCorsiWrapper._elencoCorsi)) {
			return true;
		}

		return false;
	}

	@Override
	public StagedModelType getStagedModelType() {
		return _elencoCorsi.getStagedModelType();
	}

	@Override
	public ElencoCorsi getWrappedModel() {
		return _elencoCorsi;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _elencoCorsi.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _elencoCorsi.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_elencoCorsi.resetOriginalValues();
	}

	private final ElencoCorsi _elencoCorsi;
}