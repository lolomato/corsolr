/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link course.service.http.ElencoCorsiServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see course.service.http.ElencoCorsiServiceSoap
 * @generated
 */
@ProviderType
public class ElencoCorsiSoap implements Serializable {
	public static ElencoCorsiSoap toSoapModel(ElencoCorsi model) {
		ElencoCorsiSoap soapModel = new ElencoCorsiSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setElencoCorsiId(model.getElencoCorsiId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setCompanyId(model.getCompanyId());
		soapModel.setUserId(model.getUserId());
		soapModel.setUserName(model.getUserName());
		soapModel.setCreateDate(model.getCreateDate());
		soapModel.setModifiedDate(model.getModifiedDate());
		soapModel.setNomeCorso(model.getNomeCorso());
		soapModel.setProf(model.getProf());

		return soapModel;
	}

	public static ElencoCorsiSoap[] toSoapModels(ElencoCorsi[] models) {
		ElencoCorsiSoap[] soapModels = new ElencoCorsiSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static ElencoCorsiSoap[][] toSoapModels(ElencoCorsi[][] models) {
		ElencoCorsiSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new ElencoCorsiSoap[models.length][models[0].length];
		}
		else {
			soapModels = new ElencoCorsiSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static ElencoCorsiSoap[] toSoapModels(List<ElencoCorsi> models) {
		List<ElencoCorsiSoap> soapModels = new ArrayList<ElencoCorsiSoap>(models.size());

		for (ElencoCorsi model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new ElencoCorsiSoap[soapModels.size()]);
	}

	public ElencoCorsiSoap() {
	}

	public long getPrimaryKey() {
		return _elencoCorsiId;
	}

	public void setPrimaryKey(long pk) {
		setElencoCorsiId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getElencoCorsiId() {
		return _elencoCorsiId;
	}

	public void setElencoCorsiId(long elencoCorsiId) {
		_elencoCorsiId = elencoCorsiId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getCompanyId() {
		return _companyId;
	}

	public void setCompanyId(long companyId) {
		_companyId = companyId;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserName() {
		return _userName;
	}

	public void setUserName(String userName) {
		_userName = userName;
	}

	public Date getCreateDate() {
		return _createDate;
	}

	public void setCreateDate(Date createDate) {
		_createDate = createDate;
	}

	public Date getModifiedDate() {
		return _modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		_modifiedDate = modifiedDate;
	}

	public String getNomeCorso() {
		return _nomeCorso;
	}

	public void setNomeCorso(String nomeCorso) {
		_nomeCorso = nomeCorso;
	}

	public String getProf() {
		return _prof;
	}

	public void setProf(String prof) {
		_prof = prof;
	}

	private String _uuid;
	private long _elencoCorsiId;
	private long _groupId;
	private long _companyId;
	private long _userId;
	private String _userName;
	private Date _createDate;
	private Date _modifiedDate;
	private String _nomeCorso;
	private String _prof;
}