/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import course.exception.NoSuchElencoCorsiException;

import course.model.ElencoCorsi;

/**
 * The persistence interface for the elenco corsi service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see course.service.persistence.impl.ElencoCorsiPersistenceImpl
 * @see ElencoCorsiUtil
 * @generated
 */
@ProviderType
public interface ElencoCorsiPersistence extends BasePersistence<ElencoCorsi> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link ElencoCorsiUtil} to access the elenco corsi persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Returns all the elenco corsis where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid(String uuid);

	/**
	* Returns a range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid(String uuid, int start,
		int end);

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid(String uuid, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUuid_First(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUuid_Last(String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi[] findByUuid_PrevAndNext(long elencoCorsiId,
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Removes all the elenco corsis where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public void removeByUuid(String uuid);

	/**
	* Returns the number of elenco corsis where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching elenco corsis
	*/
	public int countByUuid(String uuid);

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchElencoCorsiException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByUUID_G(String uuid, long groupId)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUUID_G(String uuid, long groupId);

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache);

	/**
	* Removes the elenco corsi where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the elenco corsi that was removed
	*/
	public ElencoCorsi removeByUUID_G(String uuid, long groupId)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the number of elenco corsis where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching elenco corsis
	*/
	public int countByUUID_G(String uuid, long groupId);

	/**
	* Returns all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid_C(String uuid, long companyId);

	/**
	* Returns a range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid_C(String uuid,
		long companyId, int start, int end);

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByUuid_C(String uuid,
		long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUuid_C_First(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByUuid_C_Last(String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi[] findByUuid_C_PrevAndNext(long elencoCorsiId,
		String uuid, long companyId,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Removes all the elenco corsis where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public void removeByUuid_C(String uuid, long companyId);

	/**
	* Returns the number of elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching elenco corsis
	*/
	public int countByUuid_C(String uuid, long companyId);

	/**
	* Returns all the elenco corsis where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @return the matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByNomeCorso(String nomeCorso);

	/**
	* Returns a range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end);

	/**
	* Returns an ordered range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns an ordered range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public java.util.List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Returns the first elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByNomeCorso_First(String nomeCorso,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the first elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByNomeCorso_First(String nomeCorso,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the last elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public ElencoCorsi findByNomeCorso_Last(String nomeCorso,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the last elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public ElencoCorsi fetchByNomeCorso_Last(String nomeCorso,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi[] findByNomeCorso_PrevAndNext(long elencoCorsiId,
		String nomeCorso,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator)
		throws NoSuchElencoCorsiException;

	/**
	* Removes all the elenco corsis where nomeCorso = &#63; from the database.
	*
	* @param nomeCorso the nome corso
	*/
	public void removeByNomeCorso(String nomeCorso);

	/**
	* Returns the number of elenco corsis where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @return the number of matching elenco corsis
	*/
	public int countByNomeCorso(String nomeCorso);

	/**
	* Caches the elenco corsi in the entity cache if it is enabled.
	*
	* @param elencoCorsi the elenco corsi
	*/
	public void cacheResult(ElencoCorsi elencoCorsi);

	/**
	* Caches the elenco corsis in the entity cache if it is enabled.
	*
	* @param elencoCorsis the elenco corsis
	*/
	public void cacheResult(java.util.List<ElencoCorsi> elencoCorsis);

	/**
	* Creates a new elenco corsi with the primary key. Does not add the elenco corsi to the database.
	*
	* @param elencoCorsiId the primary key for the new elenco corsi
	* @return the new elenco corsi
	*/
	public ElencoCorsi create(long elencoCorsiId);

	/**
	* Removes the elenco corsi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi that was removed
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi remove(long elencoCorsiId)
		throws NoSuchElencoCorsiException;

	public ElencoCorsi updateImpl(ElencoCorsi elencoCorsi);

	/**
	* Returns the elenco corsi with the primary key or throws a {@link NoSuchElencoCorsiException} if it could not be found.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi findByPrimaryKey(long elencoCorsiId)
		throws NoSuchElencoCorsiException;

	/**
	* Returns the elenco corsi with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi, or <code>null</code> if a elenco corsi with the primary key could not be found
	*/
	public ElencoCorsi fetchByPrimaryKey(long elencoCorsiId);

	@Override
	public java.util.Map<java.io.Serializable, ElencoCorsi> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys);

	/**
	* Returns all the elenco corsis.
	*
	* @return the elenco corsis
	*/
	public java.util.List<ElencoCorsi> findAll();

	/**
	* Returns a range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of elenco corsis
	*/
	public java.util.List<ElencoCorsi> findAll(int start, int end);

	/**
	* Returns an ordered range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of elenco corsis
	*/
	public java.util.List<ElencoCorsi> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator);

	/**
	* Returns an ordered range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of elenco corsis
	*/
	public java.util.List<ElencoCorsi> findAll(int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache);

	/**
	* Removes all the elenco corsis from the database.
	*/
	public void removeAll();

	/**
	* Returns the number of elenco corsis.
	*
	* @return the number of elenco corsis
	*/
	public int countAll();

	@Override
	public java.util.Set<String> getBadColumnNames();
}