/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ElencoCorsiService}.
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiService
 * @generated
 */
@ProviderType
public class ElencoCorsiServiceWrapper implements ElencoCorsiService,
	ServiceWrapper<ElencoCorsiService> {
	public ElencoCorsiServiceWrapper(ElencoCorsiService elencoCorsiService) {
		_elencoCorsiService = elencoCorsiService;
	}

	@Override
	public course.model.ElencoCorsi addLibro(long userId,
		com.liferay.portal.kernel.service.ServiceContext ctx, String nomeCorso,
		String nomeProf)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiService.addLibro(userId, ctx, nomeCorso, nomeProf);
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _elencoCorsiService.getOSGiServiceIdentifier();
	}

	@Override
	public ElencoCorsiService getWrappedService() {
		return _elencoCorsiService;
	}

	@Override
	public void setWrappedService(ElencoCorsiService elencoCorsiService) {
		_elencoCorsiService = elencoCorsiService;
	}

	private ElencoCorsiService _elencoCorsiService;
}