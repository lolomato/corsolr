/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.annotation.ImplementationClassName;
import com.liferay.portal.kernel.model.PersistedModel;
import com.liferay.portal.kernel.util.Accessor;

/**
 * The extended model interface for the ElencoCorsi service. Represents a row in the &quot;Unina_ElencoCorsi&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiModel
 * @see course.model.impl.ElencoCorsiImpl
 * @see course.model.impl.ElencoCorsiModelImpl
 * @generated
 */
@ImplementationClassName("course.model.impl.ElencoCorsiImpl")
@ProviderType
public interface ElencoCorsi extends ElencoCorsiModel, PersistedModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this interface directly. Add methods to {@link course.model.impl.ElencoCorsiImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
	 */
	public static final Accessor<ElencoCorsi, Long> ELENCO_CORSI_ID_ACCESSOR = new Accessor<ElencoCorsi, Long>() {
			@Override
			public Long get(ElencoCorsi elencoCorsi) {
				return elencoCorsi.getElencoCorsiId();
			}

			@Override
			public Class<Long> getAttributeClass() {
				return Long.class;
			}

			@Override
			public Class<ElencoCorsi> getTypeClass() {
				return ElencoCorsi.class;
			}
		};
}