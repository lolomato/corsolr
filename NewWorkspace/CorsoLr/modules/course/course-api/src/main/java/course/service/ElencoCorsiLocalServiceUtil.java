/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service;

import aQute.bnd.annotation.ProviderType;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

/**
 * Provides the local service utility for ElencoCorsi. This utility wraps
 * {@link course.service.impl.ElencoCorsiLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiLocalService
 * @see course.service.base.ElencoCorsiLocalServiceBaseImpl
 * @see course.service.impl.ElencoCorsiLocalServiceImpl
 * @generated
 */
@ProviderType
public class ElencoCorsiLocalServiceUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Add custom service methods to {@link course.service.impl.ElencoCorsiLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	* Adds the elenco corsi to the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was added
	*/
	public static course.model.ElencoCorsi addElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return getService().addElencoCorsi(elencoCorsi);
	}

	/**
	* Creates a new elenco corsi with the primary key. Does not add the elenco corsi to the database.
	*
	* @param elencoCorsiId the primary key for the new elenco corsi
	* @return the new elenco corsi
	*/
	public static course.model.ElencoCorsi createElencoCorsi(long elencoCorsiId) {
		return getService().createElencoCorsi(elencoCorsiId);
	}

	/**
	* Deletes the elenco corsi from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was removed
	*/
	public static course.model.ElencoCorsi deleteElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return getService().deleteElencoCorsi(elencoCorsi);
	}

	/**
	* Deletes the elenco corsi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi that was removed
	* @throws PortalException if a elenco corsi with the primary key could not be found
	*/
	public static course.model.ElencoCorsi deleteElencoCorsi(long elencoCorsiId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deleteElencoCorsi(elencoCorsiId);
	}

	/**
	* @throws PortalException
	*/
	public static com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().deletePersistedModel(persistedModel);
	}

	public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return getService().dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return getService().dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	public static <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return getService()
				   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return getService().dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	public static long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return getService().dynamicQueryCount(dynamicQuery, projection);
	}

	public static course.model.ElencoCorsi fetchElencoCorsi(long elencoCorsiId) {
		return getService().fetchElencoCorsi(elencoCorsiId);
	}

	/**
	* Returns the elenco corsi matching the UUID and group.
	*
	* @param uuid the elenco corsi's UUID
	* @param groupId the primary key of the group
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static course.model.ElencoCorsi fetchElencoCorsiByUuidAndGroupId(
		String uuid, long groupId) {
		return getService().fetchElencoCorsiByUuidAndGroupId(uuid, groupId);
	}

	public static com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return getService().getActionableDynamicQuery();
	}

	/**
	* Returns the elenco corsi with the primary key.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi
	* @throws PortalException if a elenco corsi with the primary key could not be found
	*/
	public static course.model.ElencoCorsi getElencoCorsi(long elencoCorsiId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getElencoCorsi(elencoCorsiId);
	}

	/**
	* Returns the elenco corsi matching the UUID and group.
	*
	* @param uuid the elenco corsi's UUID
	* @param groupId the primary key of the group
	* @return the matching elenco corsi
	* @throws PortalException if a matching elenco corsi could not be found
	*/
	public static course.model.ElencoCorsi getElencoCorsiByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getElencoCorsiByUuidAndGroupId(uuid, groupId);
	}

	/**
	* Returns a range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of elenco corsis
	*/
	public static java.util.List<course.model.ElencoCorsi> getElencoCorsis(
		int start, int end) {
		return getService().getElencoCorsis(start, end);
	}

	/**
	* Returns all the elenco corsis matching the UUID and company.
	*
	* @param uuid the UUID of the elenco corsis
	* @param companyId the primary key of the company
	* @return the matching elenco corsis, or an empty list if no matches were found
	*/
	public static java.util.List<course.model.ElencoCorsi> getElencoCorsisByUuidAndCompanyId(
		String uuid, long companyId) {
		return getService().getElencoCorsisByUuidAndCompanyId(uuid, companyId);
	}

	/**
	* Returns a range of elenco corsis matching the UUID and company.
	*
	* @param uuid the UUID of the elenco corsis
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching elenco corsis, or an empty list if no matches were found
	*/
	public static java.util.List<course.model.ElencoCorsi> getElencoCorsisByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<course.model.ElencoCorsi> orderByComparator) {
		return getService()
				   .getElencoCorsisByUuidAndCompanyId(uuid, companyId, start,
			end, orderByComparator);
	}

	/**
	* Returns the number of elenco corsis.
	*
	* @return the number of elenco corsis
	*/
	public static int getElencoCorsisCount() {
		return getService().getElencoCorsisCount();
	}

	public static com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return getService().getExportActionableDynamicQuery(portletDataContext);
	}

	public static com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return getService().getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	public static String getOSGiServiceIdentifier() {
		return getService().getOSGiServiceIdentifier();
	}

	public static com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return getService().getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the elenco corsi in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was updated
	*/
	public static course.model.ElencoCorsi updateElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return getService().updateElencoCorsi(elencoCorsi);
	}

	public static ElencoCorsiLocalService getService() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ElencoCorsiLocalService, ElencoCorsiLocalService> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ElencoCorsiLocalService.class);

		ServiceTracker<ElencoCorsiLocalService, ElencoCorsiLocalService> serviceTracker =
			new ServiceTracker<ElencoCorsiLocalService, ElencoCorsiLocalService>(bundle.getBundleContext(),
				ElencoCorsiLocalService.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}