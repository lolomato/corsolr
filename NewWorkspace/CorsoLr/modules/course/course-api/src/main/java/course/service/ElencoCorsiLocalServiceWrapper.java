/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ElencoCorsiLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiLocalService
 * @generated
 */
@ProviderType
public class ElencoCorsiLocalServiceWrapper implements ElencoCorsiLocalService,
	ServiceWrapper<ElencoCorsiLocalService> {
	public ElencoCorsiLocalServiceWrapper(
		ElencoCorsiLocalService elencoCorsiLocalService) {
		_elencoCorsiLocalService = elencoCorsiLocalService;
	}

	/**
	* Adds the elenco corsi to the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was added
	*/
	@Override
	public course.model.ElencoCorsi addElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return _elencoCorsiLocalService.addElencoCorsi(elencoCorsi);
	}

	/**
	* Creates a new elenco corsi with the primary key. Does not add the elenco corsi to the database.
	*
	* @param elencoCorsiId the primary key for the new elenco corsi
	* @return the new elenco corsi
	*/
	@Override
	public course.model.ElencoCorsi createElencoCorsi(long elencoCorsiId) {
		return _elencoCorsiLocalService.createElencoCorsi(elencoCorsiId);
	}

	/**
	* Deletes the elenco corsi from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was removed
	*/
	@Override
	public course.model.ElencoCorsi deleteElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return _elencoCorsiLocalService.deleteElencoCorsi(elencoCorsi);
	}

	/**
	* Deletes the elenco corsi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi that was removed
	* @throws PortalException if a elenco corsi with the primary key could not be found
	*/
	@Override
	public course.model.ElencoCorsi deleteElencoCorsi(long elencoCorsiId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiLocalService.deleteElencoCorsi(elencoCorsiId);
	}

	/**
	* @throws PortalException
	*/
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
		com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _elencoCorsiLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _elencoCorsiLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {
		return _elencoCorsiLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	*/
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {
		return _elencoCorsiLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {
		return _elencoCorsiLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows matching the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows matching the dynamic query
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {
		return _elencoCorsiLocalService.dynamicQueryCount(dynamicQuery,
			projection);
	}

	@Override
	public course.model.ElencoCorsi fetchElencoCorsi(long elencoCorsiId) {
		return _elencoCorsiLocalService.fetchElencoCorsi(elencoCorsiId);
	}

	/**
	* Returns the elenco corsi matching the UUID and group.
	*
	* @param uuid the elenco corsi's UUID
	* @param groupId the primary key of the group
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	@Override
	public course.model.ElencoCorsi fetchElencoCorsiByUuidAndGroupId(
		String uuid, long groupId) {
		return _elencoCorsiLocalService.fetchElencoCorsiByUuidAndGroupId(uuid,
			groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery getActionableDynamicQuery() {
		return _elencoCorsiLocalService.getActionableDynamicQuery();
	}

	/**
	* Returns the elenco corsi with the primary key.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi
	* @throws PortalException if a elenco corsi with the primary key could not be found
	*/
	@Override
	public course.model.ElencoCorsi getElencoCorsi(long elencoCorsiId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiLocalService.getElencoCorsi(elencoCorsiId);
	}

	/**
	* Returns the elenco corsi matching the UUID and group.
	*
	* @param uuid the elenco corsi's UUID
	* @param groupId the primary key of the group
	* @return the matching elenco corsi
	* @throws PortalException if a matching elenco corsi could not be found
	*/
	@Override
	public course.model.ElencoCorsi getElencoCorsiByUuidAndGroupId(
		String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiLocalService.getElencoCorsiByUuidAndGroupId(uuid,
			groupId);
	}

	/**
	* Returns a range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link course.model.impl.ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of elenco corsis
	*/
	@Override
	public java.util.List<course.model.ElencoCorsi> getElencoCorsis(int start,
		int end) {
		return _elencoCorsiLocalService.getElencoCorsis(start, end);
	}

	/**
	* Returns all the elenco corsis matching the UUID and company.
	*
	* @param uuid the UUID of the elenco corsis
	* @param companyId the primary key of the company
	* @return the matching elenco corsis, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<course.model.ElencoCorsi> getElencoCorsisByUuidAndCompanyId(
		String uuid, long companyId) {
		return _elencoCorsiLocalService.getElencoCorsisByUuidAndCompanyId(uuid,
			companyId);
	}

	/**
	* Returns a range of elenco corsis matching the UUID and company.
	*
	* @param uuid the UUID of the elenco corsis
	* @param companyId the primary key of the company
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the range of matching elenco corsis, or an empty list if no matches were found
	*/
	@Override
	public java.util.List<course.model.ElencoCorsi> getElencoCorsisByUuidAndCompanyId(
		String uuid, long companyId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<course.model.ElencoCorsi> orderByComparator) {
		return _elencoCorsiLocalService.getElencoCorsisByUuidAndCompanyId(uuid,
			companyId, start, end, orderByComparator);
	}

	/**
	* Returns the number of elenco corsis.
	*
	* @return the number of elenco corsis
	*/
	@Override
	public int getElencoCorsisCount() {
		return _elencoCorsiLocalService.getElencoCorsisCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ExportActionableDynamicQuery getExportActionableDynamicQuery(
		com.liferay.exportimport.kernel.lar.PortletDataContext portletDataContext) {
		return _elencoCorsiLocalService.getExportActionableDynamicQuery(portletDataContext);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery getIndexableActionableDynamicQuery() {
		return _elencoCorsiLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	* Returns the OSGi service identifier.
	*
	* @return the OSGi service identifier
	*/
	@Override
	public String getOSGiServiceIdentifier() {
		return _elencoCorsiLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {
		return _elencoCorsiLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Updates the elenco corsi in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsi the elenco corsi
	* @return the elenco corsi that was updated
	*/
	@Override
	public course.model.ElencoCorsi updateElencoCorsi(
		course.model.ElencoCorsi elencoCorsi) {
		return _elencoCorsiLocalService.updateElencoCorsi(elencoCorsi);
	}

	@Override
	public ElencoCorsiLocalService getWrappedService() {
		return _elencoCorsiLocalService;
	}

	@Override
	public void setWrappedService(
		ElencoCorsiLocalService elencoCorsiLocalService) {
		_elencoCorsiLocalService = elencoCorsiLocalService;
	}

	private ElencoCorsiLocalService _elencoCorsiLocalService;
}