/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package course.service.persistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import course.model.ElencoCorsi;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import org.osgi.util.tracker.ServiceTracker;

import java.util.List;

/**
 * The persistence utility for the elenco corsi service. This utility wraps {@link course.service.persistence.impl.ElencoCorsiPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ElencoCorsiPersistence
 * @see course.service.persistence.impl.ElencoCorsiPersistenceImpl
 * @generated
 */
@ProviderType
public class ElencoCorsiUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(ElencoCorsi elencoCorsi) {
		getPersistence().clearCache(elencoCorsi);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<ElencoCorsi> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<ElencoCorsi> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<ElencoCorsi> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static ElencoCorsi update(ElencoCorsi elencoCorsi) {
		return getPersistence().update(elencoCorsi);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static ElencoCorsi update(ElencoCorsi elencoCorsi,
		ServiceContext serviceContext) {
		return getPersistence().update(elencoCorsi, serviceContext);
	}

	/**
	* Returns all the elenco corsis where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	* Returns a range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid(String uuid, int start, int end,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid(String uuid, int start, int end,
		OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid(uuid, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByUuid_First(String uuid,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUuid_First(String uuid,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByUuid_Last(String uuid,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUuid_Last(String uuid,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where uuid = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param uuid the uuid
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi[] findByUuid_PrevAndNext(long elencoCorsiId,
		String uuid, OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByUuid_PrevAndNext(elencoCorsiId, uuid,
			orderByComparator);
	}

	/**
	* Removes all the elenco corsis where uuid = &#63; from the database.
	*
	* @param uuid the uuid
	*/
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	* Returns the number of elenco corsis where uuid = &#63;.
	*
	* @param uuid the uuid
	* @return the number of matching elenco corsis
	*/
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or throws a {@link NoSuchElencoCorsiException} if it could not be found.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByUUID_G(String uuid, long groupId)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	* Returns the elenco corsi where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUUID_G(String uuid, long groupId,
		boolean retrieveFromCache) {
		return getPersistence().fetchByUUID_G(uuid, groupId, retrieveFromCache);
	}

	/**
	* Removes the elenco corsi where uuid = &#63; and groupId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the elenco corsi that was removed
	*/
	public static ElencoCorsi removeByUUID_G(String uuid, long groupId)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	* Returns the number of elenco corsis where uuid = &#63; and groupId = &#63;.
	*
	* @param uuid the uuid
	* @param groupId the group ID
	* @return the number of matching elenco corsis
	*/
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	* Returns all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid_C(String uuid, long companyId) {
		return getPersistence().findByUuid_C(uuid, companyId);
	}

	/**
	* Returns a range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid_C(String uuid, long companyId,
		int start, int end) {
		return getPersistence().findByUuid_C(uuid, companyId, start, end);
	}

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByUuid_C(String uuid, long companyId,
		int start, int end, OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByUuid_C(uuid, companyId, start, end,
			orderByComparator, retrieveFromCache);
	}

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the first elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUuid_C_First(String uuid, long companyId,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_First(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByUuid_C_Last(String uuid, long companyId,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .fetchByUuid_C_Last(uuid, companyId, orderByComparator);
	}

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where uuid = &#63; and companyId = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param uuid the uuid
	* @param companyId the company ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi[] findByUuid_C_PrevAndNext(long elencoCorsiId,
		String uuid, long companyId,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByUuid_C_PrevAndNext(elencoCorsiId, uuid, companyId,
			orderByComparator);
	}

	/**
	* Removes all the elenco corsis where uuid = &#63; and companyId = &#63; from the database.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	*/
	public static void removeByUuid_C(String uuid, long companyId) {
		getPersistence().removeByUuid_C(uuid, companyId);
	}

	/**
	* Returns the number of elenco corsis where uuid = &#63; and companyId = &#63;.
	*
	* @param uuid the uuid
	* @param companyId the company ID
	* @return the number of matching elenco corsis
	*/
	public static int countByUuid_C(String uuid, long companyId) {
		return getPersistence().countByUuid_C(uuid, companyId);
	}

	/**
	* Returns all the elenco corsis where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @return the matching elenco corsis
	*/
	public static List<ElencoCorsi> findByNomeCorso(String nomeCorso) {
		return getPersistence().findByNomeCorso(nomeCorso);
	}

	/**
	* Returns a range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end) {
		return getPersistence().findByNomeCorso(nomeCorso, start, end);
	}

	/**
	* Returns an ordered range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end, OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .findByNomeCorso(nomeCorso, start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the elenco corsis where nomeCorso = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param nomeCorso the nome corso
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of matching elenco corsis
	*/
	public static List<ElencoCorsi> findByNomeCorso(String nomeCorso,
		int start, int end, OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findByNomeCorso(nomeCorso, start, end, orderByComparator,
			retrieveFromCache);
	}

	/**
	* Returns the first elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByNomeCorso_First(String nomeCorso,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByNomeCorso_First(nomeCorso, orderByComparator);
	}

	/**
	* Returns the first elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByNomeCorso_First(String nomeCorso,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .fetchByNomeCorso_First(nomeCorso, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi
	* @throws NoSuchElencoCorsiException if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi findByNomeCorso_Last(String nomeCorso,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByNomeCorso_Last(nomeCorso, orderByComparator);
	}

	/**
	* Returns the last elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching elenco corsi, or <code>null</code> if a matching elenco corsi could not be found
	*/
	public static ElencoCorsi fetchByNomeCorso_Last(String nomeCorso,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence()
				   .fetchByNomeCorso_Last(nomeCorso, orderByComparator);
	}

	/**
	* Returns the elenco corsis before and after the current elenco corsi in the ordered set where nomeCorso = &#63;.
	*
	* @param elencoCorsiId the primary key of the current elenco corsi
	* @param nomeCorso the nome corso
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi[] findByNomeCorso_PrevAndNext(
		long elencoCorsiId, String nomeCorso,
		OrderByComparator<ElencoCorsi> orderByComparator)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence()
				   .findByNomeCorso_PrevAndNext(elencoCorsiId, nomeCorso,
			orderByComparator);
	}

	/**
	* Removes all the elenco corsis where nomeCorso = &#63; from the database.
	*
	* @param nomeCorso the nome corso
	*/
	public static void removeByNomeCorso(String nomeCorso) {
		getPersistence().removeByNomeCorso(nomeCorso);
	}

	/**
	* Returns the number of elenco corsis where nomeCorso = &#63;.
	*
	* @param nomeCorso the nome corso
	* @return the number of matching elenco corsis
	*/
	public static int countByNomeCorso(String nomeCorso) {
		return getPersistence().countByNomeCorso(nomeCorso);
	}

	/**
	* Caches the elenco corsi in the entity cache if it is enabled.
	*
	* @param elencoCorsi the elenco corsi
	*/
	public static void cacheResult(ElencoCorsi elencoCorsi) {
		getPersistence().cacheResult(elencoCorsi);
	}

	/**
	* Caches the elenco corsis in the entity cache if it is enabled.
	*
	* @param elencoCorsis the elenco corsis
	*/
	public static void cacheResult(List<ElencoCorsi> elencoCorsis) {
		getPersistence().cacheResult(elencoCorsis);
	}

	/**
	* Creates a new elenco corsi with the primary key. Does not add the elenco corsi to the database.
	*
	* @param elencoCorsiId the primary key for the new elenco corsi
	* @return the new elenco corsi
	*/
	public static ElencoCorsi create(long elencoCorsiId) {
		return getPersistence().create(elencoCorsiId);
	}

	/**
	* Removes the elenco corsi with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi that was removed
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi remove(long elencoCorsiId)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().remove(elencoCorsiId);
	}

	public static ElencoCorsi updateImpl(ElencoCorsi elencoCorsi) {
		return getPersistence().updateImpl(elencoCorsi);
	}

	/**
	* Returns the elenco corsi with the primary key or throws a {@link NoSuchElencoCorsiException} if it could not be found.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi
	* @throws NoSuchElencoCorsiException if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi findByPrimaryKey(long elencoCorsiId)
		throws course.exception.NoSuchElencoCorsiException {
		return getPersistence().findByPrimaryKey(elencoCorsiId);
	}

	/**
	* Returns the elenco corsi with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param elencoCorsiId the primary key of the elenco corsi
	* @return the elenco corsi, or <code>null</code> if a elenco corsi with the primary key could not be found
	*/
	public static ElencoCorsi fetchByPrimaryKey(long elencoCorsiId) {
		return getPersistence().fetchByPrimaryKey(elencoCorsiId);
	}

	public static java.util.Map<java.io.Serializable, ElencoCorsi> fetchByPrimaryKeys(
		java.util.Set<java.io.Serializable> primaryKeys) {
		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	* Returns all the elenco corsis.
	*
	* @return the elenco corsis
	*/
	public static List<ElencoCorsi> findAll() {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @return the range of elenco corsis
	*/
	public static List<ElencoCorsi> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of elenco corsis
	*/
	public static List<ElencoCorsi> findAll(int start, int end,
		OrderByComparator<ElencoCorsi> orderByComparator) {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Returns an ordered range of all the elenco corsis.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link ElencoCorsiModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of elenco corsis
	* @param end the upper bound of the range of elenco corsis (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @param retrieveFromCache whether to retrieve from the finder cache
	* @return the ordered range of elenco corsis
	*/
	public static List<ElencoCorsi> findAll(int start, int end,
		OrderByComparator<ElencoCorsi> orderByComparator,
		boolean retrieveFromCache) {
		return getPersistence()
				   .findAll(start, end, orderByComparator, retrieveFromCache);
	}

	/**
	* Removes all the elenco corsis from the database.
	*/
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of elenco corsis.
	*
	* @return the number of elenco corsis
	*/
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static java.util.Set<String> getBadColumnNames() {
		return getPersistence().getBadColumnNames();
	}

	public static ElencoCorsiPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<ElencoCorsiPersistence, ElencoCorsiPersistence> _serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(ElencoCorsiPersistence.class);

		ServiceTracker<ElencoCorsiPersistence, ElencoCorsiPersistence> serviceTracker =
			new ServiceTracker<ElencoCorsiPersistence, ElencoCorsiPersistence>(bundle.getBundleContext(),
				ElencoCorsiPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}
}